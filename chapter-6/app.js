const express = require('express');
const app = express();
const userRouter = require("./users/users.route");
const UserController = require('./users/users.controller')
const mainRouter = require("./main/main.route");
const port = 8000;

app.use(express.json());
app.use("/users", userRouter);
app.use("/RPS-Web", mainRouter);
app.listen(port, UserController.dbConn())