'use strict';
const {
  Model
} = require('sequelize');
const moment = require('moment')
module.exports = (sequelize, DataTypes) => {
  class user_game extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      user_game.hasOne(models.user_bio, {foreignKey : "user_id"});
      user_game.hasMany(models.user_history, {foreignKey : "user_id"})
    }
  }
  user_game.init({
    username: {
      type : DataTypes.STRING
    },
    email: {
      type : DataTypes.STRING
    },
    password: {
      type : DataTypes.STRING
    },
    createdAt: {
      type : DataTypes.DATE,
      get() {
        return moment(this.getDataValue('createdAt')).format('DD/MM/YYYY h:mm:ss');
      }
    }
  }, {
    sequelize,
    modelName: 'user_game',
  });
  return user_game;
};