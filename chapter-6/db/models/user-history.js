'use strict';
const {
  Model
} = require('sequelize');
const moment = require('moment')
module.exports = (sequelize, DataTypes) => {
  class user_history extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      user_history.belongsTo(models.user_game, {foreignKey : "user_id"});
    }
  }
  user_history.init({
    status: {
      type : DataTypes.STRING
    },
    createdAt : {
      type : DataTypes.DATE,
      get() {
        return moment(this.getDataValue('createdAt')).format('DD/MM/YYYY h:mm:ss');
      }
    }
  }, {
    sequelize,
    modelName: 'user_history',
  });
  return user_history;
};