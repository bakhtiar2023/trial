'use strict';
const {
  Model
} = require('sequelize');
const moment = require('moment');
module.exports = (sequelize, DataTypes) => {
  class user_bio extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      user_bio.belongsTo(models.user_game, {foreignKey: "user_id"});
    }
  }
  user_bio.init({
    fullname: {type : DataTypes.STRING},
    address: {type : DataTypes.STRING},
    phone_number: {type : DataTypes.STRING},
    date_of_birth: {type : DataTypes.STRING},
    updatedAt : {
      type : DataTypes.DATE,
      get() {
        return moment(this.getDataValue('createdAt')).format('DD/MM/YYYY h:mm:ss');
      }
    }
  }, {
    sequelize,
    modelName: 'user_bio',
  });
  return user_bio;
};