'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.addConstraint('user_bios', {
      fields :  ['user_id'],
      type : 'foreign key',
      name : 'usergame_userbio_association',
      references : {
        table : 'user_games',
        field : 'id'
      }
    })
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.removeConstraint('user_bios', 'user_id')
  }
};
