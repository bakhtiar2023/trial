const userValidator = require("./users.validator");
const { validationResult } = require("express-validator");
const { sequelize } = require("../db/models");

class UserController {
  getAllUser = async (req, res) => {
    const resultGetAllUser = await userValidator.getAllUserValidator(res);
    return resultGetAllUser;
  };

  getLogin = async (req, res) => {
    const { username, email, password } = req.body;
    const errors = validationResult(req);
    const loginResult = await userValidator.loginValidator(
      res,
      errors,
      username,
      email,
      password
    );
    return loginResult;
  };

  getSignup = async (req, res) => {
    const { username, email, password } = req.body;
    const errors = validationResult(req);
    const signupResult = await userValidator.signupValidator(
      res,
      errors,
      username,
      email,
      password
    );
    return signupResult;
  };

  getUserBio = async (req, res) => {
    const { user_id } = req.params;
    const resultGetBio = await userValidator.getDetailBioValidator(
      res,
      user_id
    );
    return resultGetBio;
  };

  addUpdateBio = async (req, res) => {
    const { user_id } = req.params;
    const { fullname, address, phone_number, date_of_birth } = req.body;
    const errors = validationResult(req);
    const resultAddUpdateBio = await userValidator.addUpdateUserBioValidator(
      res,
      errors,
      fullname,
      address,
      phone_number,
      date_of_birth,
      user_id
    );
    return resultAddUpdateBio;
  };

  addHistory = async (req, res) => {
    const { status, user_id } = req.body;
    const errors = validationResult(req);
    const resultAddUserHistory = await userValidator.historyValidator(
      res,
      errors,
      status,
      user_id
    );
    return resultAddUserHistory;
  };

  findHistory = async (req, res) => {
    const { user_id } = req.params;
    const resultFindHistory = await userValidator.findHistoryValidator(
      res,
      user_id
    );
    return resultFindHistory;
  };

  dbConn = async () => {
    await sequelize.authenticate();
    console.log("Database connected!!");
  };
}

module.exports = new UserController();
