const md5 = require("md5");
const { user_game, user_bio, user_history } = require("../db/models");
class UserModel {
  loadData = async () => {
    const daftarUsers = await user_game.findAll();
    return daftarUsers;
  };

  addData = async (username, email, password) => {
    await user_game.create({
      username,
      email,
      password: md5(password),
    });
  };

  addBiodata = async (
    fullname,
    address,
    phone_number,
    date_of_birth,
    user_id
  ) => {
    await user_bio.create({
      fullname,
      address,
      phone_number,
      user_id,
      date_of_birth,
    });
  };

  findUsername = async (username) => {
    try {
      const findUsername = await user_game.findOne({
        where: { username: username },
      });
      return findUsername;
    } catch (error) {
      return error;
    }
  };

  getDataById = async (id) => {
    try {
      const findId = await user_game.findOne({ where: { id: id } });
      return findId;
    } catch (error) {
      return error;
    }
  };

  findEmail = async (email) => {
    try {
      const findEmail = await user_game.findOne({ where: { email: email } });
      return findEmail;
    } catch (error) {
      return error;
    }
  };

  findPassword = async (password) => {
    try {
      const findPassword = await user_game.findOne({
        where: { password: md5(password) },
      });
      return findPassword;
    } catch (error) {}
  };

  findBioById = async (user_id) => {
    try {
      const findBioId = await user_bio.findOne({ where: { user_id } });
      return findBioId;
    } catch (error) {
      return error.where;
    }
  };

  findDetailBio = async (user_id) => {
    try {
      const findBioId = await user_bio.findOne({
        include: "user_game",
        where: { user_id },
      });
      return findBioId;
    } catch (error) {
      return error.where;
    }
  };

  updateNoPhone = async (fullname, address, user_id) => {
    const resultUpdateNoPhone = await user_bio.update(
      { fullname, address },
      { where: { user_id } }
    );
    return resultUpdateNoPhone;
  };

  updateNoFullname = async (address, phone_number, date_of_birth, user_id) => {
    const resultUpdateNoFullname = await user_bio.update(
      { address, phone_number, date_of_birth },
      { where: { user_id } }
    );
    return resultUpdateNoFullname;
  };

  updateNoAddress = async (fullname, phone_number, date_of_birth, user_id) => {
    const resultUpdateNoAddress = await user_bio.update(
      { fullname, phone_number, date_of_birth },
      { where: { user_id } }
    );
    return resultUpdateNoAddress;
  };

  updateAllBio = async (
    fullname,
    address,
    phone_number,
    date_of_birth,
    user_id
  ) => {
    const resultUpdateAll = await user_bio.update(
      { fullname, address, phone_number, date_of_birth },
      { where: { user_id } }
    );
    return resultUpdateAll;
  };

  updateAddress = async (address, user_id) => {
    const resultUpdateAddress = await user_bio.update(
      { address },
      { where: { user_id } }
    );
    return resultUpdateAddress;
  };

  updateFullname = async (fullname, user_id) => {
    const resultUpdateFullname = await user_bio.update(
      { fullname },
      { where: { user_id } }
    );
    return resultUpdateFullname;
  };

  updatePhoneNumber = async (phone_number, date_of_birth, user_id) => {
    const resultUpdatePhoneNumber = await user_bio.update(
      { phone_number, date_of_birth },
      { where: { user_id } }
    );
    return resultUpdatePhoneNumber;
  };

  addHistory = async (status, user_id) => {
    const resultAddHistory = await user_history.create({ status, user_id });
    return resultAddHistory;
  };

  getHistoryById = async (user_id) => {
    try {
      const resultFindHistory = await user_history.findOne({
        where: { user_id },
      });
      return resultFindHistory;
    } catch (error) {
      return error.where;
    }
  };

  findAllHistory = async (user_id) => {
    try {
      const resultFindAll = await user_history.findAll({
        include: "user_game",
        distinct: user_id,
        where: { user_id },
      });
      return resultFindAll;
    } catch (error) {
      return error.where;
    }
  };
}

module.exports = new UserModel();
