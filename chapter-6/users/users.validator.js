const userModel = require("./users.model");
const { body } = require("express-validator");
const md5 = require("md5");

class UserValidator {
  signupValidRules = () => {
    return [
      body("username").notEmpty().withMessage("Mohon masukkan username anda"),

      body("email")
        .notEmpty()
        .withMessage("Mohon masukkan email anda")
        .isEmail()
        .withMessage("Format email yang anda masukkan tidak valid"),

      body("password")
        .notEmpty()
        .withMessage("Mohon masukkan password anda")
        .isLength({
          min: 6,
        })
        .withMessage("Mohon masukkan password min.6 character"),
    ];
  };

  loginValidRules = () => {
    return [body("password").notEmpty().withMessage("Masukkan password anda")];
  };

  addUpdateBioValidRules = () => {
    return [
      body("phone_number")
        .isMobilePhone("id-ID")
        .withMessage("Nomor hp yang anda masukkan tidak valid"),
      body("date_of_birth")
        .isDate("dd-mm-yyyy")
        .withMessage("Format tanggal lahir tidak valid, format : dd-mm-yyyy"),
    ];
  };

  historyValidRules = () => {
    return [
      body("status")
        .notEmpty()
        .withMessage("Masukkan status pemain")
        .isIn(["menang", "kalah", "seri"])
        .withMessage("Masukkan pilihan status : menang/seri/kalah"),
      body("user_id")
        .notEmpty()
        .withMessage(
          "Masukkan id yang ditampilkan pada URL localhost:8000/users method GET"
        ),
    ];
  };

  getAllUserValidator = async (res) => {
    const daftarUsers = await userModel.loadData();
    if (daftarUsers.length < 1) {
      return res.status(404).json({
        message:
          "Data tidak ditemukan, silahkan mendaftar pada URL localhost:8000/users/signup Method POST",
      });
    } else {
      const arrdata = [];
      daftarUsers.forEach((daftarUser) => {
        return arrdata.push({
          userId: daftarUser.id,
          username: daftarUser.username,
          email: daftarUser.email,
          password: daftarUser.password,
          joined_since: daftarUser.createdAt,
        });
      });
      return res.status(200).json(arrdata);
    }
  };

  signupValidator = async (res, errors, username, email, password) => {
    const findUsername = await userModel.findUsername(username);
    const findEmail = await userModel.findEmail(email);
    if (findUsername !== null && findEmail == null) {
      return res.status(409).send({
        error: "Username sudah terdaftar",
        message: "Mohon masukkan username lain",
      });
    } else if (findUsername == null && findEmail !== null) {
      return res
        .status(409)
        .send({ error: "Email sudah terdaftar oleh user lain" });
    } else {
      if (errors.isEmpty()) {
        if (findUsername == null && findEmail == null) {
          await userModel.addData(username, email, password);
          return res
            .status(200)
            .send({ message: `${username} berhasil mendaftar` });
        } else {
          return res
            .status(409)
            .send({ error: "Username dan Email sudah terdaftar" });
        }
      }
      const extractedErrors = [];
      errors
        .array()
        .map((err) => extractedErrors.push({ [err.param]: err.msg }));

      return res.status(400).json({
        errors: extractedErrors,
      });
    }
  };

  loginValidator = async (res, errors, username, email, password) => {
    const findUsername = await userModel.findUsername(username);
    const findPassword = await userModel.findPassword(password);
    const findEmail = await userModel.findEmail(email);
    if (email == undefined && username !== undefined) {
      if (!errors.isEmpty()) {
        const extractedErrors = [];
        errors
          .array()
          .map((err) => extractedErrors.push({ [err.param]: err.msg }));
        return res.status(400).json({ errors: extractedErrors });
      } else if (findUsername == null && findPassword !== null) {
        return res.status(404).json({
          errors:
            "User tidak ditemukan, silahkan mendaftar pada URL localhost:8000/users/signup Method POST",
        });
      } else if (findUsername !== null && findPassword == null) {
        return res.status(400).json({
          errors: "password yang anda masukkan salah",
        });
      } else {
        if (findUsername !== null || findPassword !== null) {
          if (
            username == findUsername.username &&
            md5(password) == findPassword.password
          ) {
            const id = findUsername.id;
            const dataUser = await userModel.getDataById(id);
            return res.status(200).json({
              message: "berhasil login!!",
              data_anda: {
                userId: dataUser.id,
                username: dataUser.username,
                email: dataUser.email,
                password: dataUser.password,
                joinAt: dataUser.createdAt,
              },
            });
          }
        } else {
          return res.status(404).json({
            errors:
              "User tidak ditemukan, silahkan mendaftar pada URL localhost:8000/users/signup Method POST",
          });
        }
      }
    } else if (email !== undefined && username == undefined) {
      if (!errors.isEmpty()) {
        const extractedErrors = [];
        errors
          .array()
          .map((err) => extractedErrors.push({ [err.param]: err.msg }));
        return res.status(400).json({ errors: extractedErrors });
      } else if (findEmail == null && findPassword !== null) {
        return res.status(404).json({
          errors:
            "User tidak ditemukan, silahkan mendaftar pada URL localhost:8000/users/signup Method POST",
        });
      } else if (findEmail !== null && findPassword == null) {
        return res.status(400).json({
          errors: "password yang anda masukkan salah",
        });
      } else {
        if (findEmail !== null || findPassword !== null) {
          if (
            email == findEmail.email &&
            md5(password) == findPassword.password
          ) {
            const id = findEmail.id;
            const dataUser = await userModel.getDataById(id);
            return res.status(200).json({
              message: "berhasil login!!",
              data_anda: {
                userId: dataUser.id,
                username: dataUser.username,
                email: dataUser.email,
                password: dataUser.password,
                joinAt: dataUser.createdAt,
              },
            });
          }
        } else {
          return res.status(404).json({
            errors:
              "User tidak ditemukan, silahkan mendaftar pada URL localhost:8000/users/signup Method POST",
          });
        }
      }
    } else {
      return res.status(404).json({
        errors:
          "Untuk login silahkan masukkan username atau email anda dengan password",
      });
    }
  };

  addUpdateUserBioValidator = async (
    res,
    errors,
    fullname,
    address,
    phone_number,
    date_of_birth,
    user_id
  ) => {
    const user = await userModel.getDataById(user_id);

    if (!user) {
      return res.status(404).json({
        error:
          "User tidak ditemukan, silahkan mendaftar pada URL localhost:8000/users/signup Method POST",
      });
    }

    const bio = await userModel.findBioById(user_id);

    if (!phone_number && !date_of_birth) {
      if (!bio) {
        if (!fullname || !address) {
          return res.status(400).json({
            error:
              "Untuk melakukan update silahkan masukkan data anda. berupa: fullname, address, phone_number, date_of_birth",
          });
        }

        await userModel.addBiodata(
          fullname,
          address,
          phone_number,
          date_of_birth,
          user_id
        );

        return res.status(200).json({
          message: `Biodata user ${user.username} berhasil ditambahkan!!`,
        });
      }

      if (fullname !== undefined && address !== undefined) {
        await userModel.updateNoPhone(fullname, address, user_id);
      } else if (fullname == undefined && address != undefined) {
        await userModel.updateAddress(address, user_id);
      } else if (fullname !== undefined && address == undefined) {
        await userModel.updateFullname(fullname, user_id);
      } else {
        return res.status(400).json({
          error:
            "Untuk melakukan update silahkan masukkan data anda. berupa: fullname, address, phone_number, date_of_birth",
        });
      }

      return res.status(200).json({
        message: `Biodata user ${user.username} berhasil ditambahkan!!`,
      });
    }

    if (errors.isEmpty()) {
      if (fullname == undefined && address !== undefined) {
        await userModel.updateNoFullname(
          address,
          phone_number,
          date_of_birth,
          user_id
        );
      } else if (fullname !== undefined && address == undefined) {
        await userModel.updateNoAddress(
          fullname,
          phone_number,
          date_of_birth,
          user_id
        );
      } else if (fullname == undefined && address == undefined) {
        await userModel.updatePhoneNumber(phone_number, date_of_birth, user_id);
      } else {
        await userModel.updateAllBio(
          fullname,
          address,
          phone_number,
          date_of_birth,
          user_id
        );
      }

      return res.status(200).json({
        message: `Biodata user ${user.username} berhasil diupdate!!`,
      });
    } else {
      return res.status(400).json({
        error: errors.array()[0].msg,
      });
    }
  };

  getDetailBioValidator = async (res, user_id) => {
    const resultFindById = await userModel.findDetailBio(user_id);
    const checkUser = await userModel.getDataById(user_id);
    if (checkUser == null) {
      return res.status(404).json({
        error:
          "User tidak ditemukan, silahkan mendaftar pada URL localhost:8000/users Method POST",
      });
    } else {
      if (resultFindById == undefined || resultFindById == null) {
        return res.status(404).json({
          error:
            "Detail tidak ditemukan. Untuk menampilkan detail membutuhkan biodata, silahkan input pada URL localhost:8000/users/biodata Method PUT",
        });
      } else {
        return res.status(200).json({
          userId: resultFindById.user_game.id,
          username: resultFindById.user_game.username,
          email: resultFindById.user_game.email,
          fullname: resultFindById.fullname,
          address: resultFindById.address,
          phone_number: resultFindById.phone_number,
          date_of_birth: resultFindById.date_of_birth,
          joined_since: resultFindById.user_game.createdAt,
          last_updated_at: resultFindById.updatedAt,
        });
      }
    }
  };

  historyValidator = async (res, errors, status, user_id) => {
    const checkUser = await userModel.getDataById(user_id);
    if (checkUser !== null) {
      if (errors.isEmpty()) {
        const resultAddHistory = await userModel.addHistory(status, user_id);
        return res.status(200).json({
          message: `berhasil menambahkan history pemain`,
          userID: `${user_id}`,
          username: `${checkUser.username}`,
          status: `${status}`,
        });
      } else {
        const extractedErrors = [];
        errors
          .array()
          .map((err) => extractedErrors.push({ [err.param]: err.msg }));
        return res.status(400).json({
          error: extractedErrors,
        });
      }
    } else {
      return res.status(404).json({
        error:
          "User tidak ditemukan, silahkan melakukan registrasi pada URL localhost:8000/users/signup Method POST",
      });
    }
  };

  findHistoryValidator = async (res, user_id) => {
    const checkUser = await userModel.getDataById(user_id);
    const checkHistory = await userModel.getHistoryById(user_id);
    if (checkUser == null) {
      return res.status(404).json({
        error:
          "User tidak ditemukan, silahkan mendaftar pada URL localhost:8000/users/signup Method POST",
      });
    } else {
      if (checkHistory == undefined || checkHistory == null) {
        return res.status(404).json({
          error: `History permainan user ${checkUser.username} tidak ditemukan,silahkan masukkan history pada URL localhost:8000/users/history Method PUT`,
        });
      } else {
        const findAllHistoryUser = await userModel.findAllHistory(user_id);
        const data = [];
        findAllHistoryUser.forEach((arrayData) => {
          return data.push({
            userID: arrayData.user_id,
            username: arrayData.user_game.username,
            hasil_bermain: arrayData.status,
            tanggal_bermain: arrayData.createdAt,
          });
        });
        return res.status(200).json(data);
      }
    }
  };
}

module.exports = new UserValidator();
