const express = require("express");
const userRouter = express.Router();
const userController = require("./users.controller");
const userValidator = require("./users.validator");

userRouter.get("/", userController.getAllUser);
userRouter.post(
  "/signup",
  userValidator.signupValidRules(),
  userController.getSignup
);
userRouter.post(
  "/login",
  userValidator.loginValidRules(),
  userController.getLogin
);
userRouter.put(
  "/biodata/:user_id",
  userValidator.addUpdateBioValidRules(),
  userController.addUpdateBio
);
userRouter.get("/detail/:user_id", userController.getUserBio);
userRouter.put(
  "/history",
  userValidator.historyValidRules(),
  userController.addHistory
);
userRouter.get("/history/:user_id", userController.findHistory);

module.exports = userRouter;
