const express = require('express');
const mainRouter = express.Router();
const mainController = require('./main.controller')

mainRouter.use(express.static('public'));

mainRouter.get('/', mainController.getMainPage);
mainRouter.get('/games', mainController.getGamePage)

module.exports = mainRouter